import tkinter as tk 
from tkinter import StringVar
import PIL
from PIL import Image, ImageTk
import cv2
import numpy as np
import threading
import time
from haui import userinterface
from haui import arduinoComunication
from haui import database
from haui import detector
from serial import Serial
import settings


#setting

class show_images(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.video_capture_input = cv2.VideoCapture(0)
        self.video_capture_output = cv2.VideoCapture(2)
    def run(self):
        global ui
        print("[INFO] start show...")
        while 1:
            if(self.video_capture_input.isOpened()):
                _, input_frame = self.video_capture_input.read()
                cv2.imwrite("input_frame.jpg", input_frame)
                img = cv2.imread("input_frame.jpg", 0)
                ui.set_image_input(img)
                self.video_capture_input.release()
            else:
                self.video_capture_input = cv2.VideoCapture(0)

            if(self.video_capture_output.isOpened()):
                _, output_frame = self.video_capture_output.read()
                cv2.imwrite("output_frame.jpg", output_frame)
                img = cv2.imread("output_frame.jpg", 0)
                ui.set_image_output(img)
                self.video_capture_output.release()
            else:
                self.video_capture_output = cv2.VideoCapture(2)
            # cv2.waitKey(1)
            # time.sleep(2)
        self.run()

class processing(threading.Thread):
    def __init__(self):
        self.settings = settings.settings()
        threading.Thread.__init__(self)
        self.arduino = arduinoComunication.arduino()
        self.db = database.database()
        self.detector = detector.LicensePlateRecognition()
        self.maximum_car = self.settings.MaxumunCar
    def update_total_car(self):
        total = self.db.get_total_car()
        ui.set_car_no_label(total)
        return total
    def update_revenue(self):
        revenue = self.db.get_revenue()
        ui.set_revenue(revenue)
    
    def run(self):
        global ui
        a = show_images()
        ui.set_state("Ready", "deep sky blue")
        ui.set_prices_label(0)
        self.update_total_car()
        self.update_revenue()
        if(self.settings.manual == False):
            a.start()
        while True:
            pass
            print("[INFO] waiting signal from arduino...")
            direction, id_card = self.arduino.get_data_from_arduino()
            # xe vao
            if(direction == "i"):
                img = cv2.imread("input_frame.jpg", 0)
                img_plate,data_license_plate =  self.detector.license_plate_recognition(img)
                if(img_plate is not None):
                    ui.set_image_license_input(img_plate)
                    ui.set_plate_car_input(data_license_plate)
                    if(self.update_total_car() < self.maximum_car):
                        result = self.db.add_new_car(id_card, data_license_plate)
                        if(result == 0):
                            ui.set_state("Wellcome", "green")
                        elif (result == -2):
                            ui.card_id_exists()
                        elif (result == -1):
                            ui.car_exists()
                    else:
                        ui.full_slot()
                else:
                    ui.not_found()
             # lay xe ra           
            elif (direction == "o"):
                img = cv2.imread("output_frame.jpg", 0)
                img_plate,data_license_plate =  self.detector.license_plate_recognition(img)
                if(img_plate is not None):
                    ui.set_image_license_output(img_plate)
                    ui.set_plate_car_output(data_license_plate)
                    status, color = self.db.check_id(id_card, data_license_plate)
                    # khong tim thay xe trong bai do xe
                    if(status != "NOT FOUND"):
                        ui.set_state(status, color)
                    if(status == "PASS"):
                        # xe chinh xac di ra ngoai
                        h, m, s, price = self.db.remove_car(id_card, data_license_plate, self.settings.price)
                        ui.set_time_label(h, m ,s)
                        ui.set_prices_label(price)
                        self.update_revenue()
                    # the id khong dung voi xe nay
                    else:
                        ui.set_time_label(0, 0 ,0)
                        ui.set_prices_label(0)
                # khong tim thay bien so
                else:
                    ui.not_found()
            time.sleep(1)
            self.update_total_car()
            self.update_revenue()
            self.arduino.done()
            self.arduino.release()

if __name__ == "__main__":
    global ui
    settings.settings().MaxumunCar
    frame = tk.Tk()
    ui = userinterface.setup_ui(frame, maximum_car= settings.settings().MaxumunCar)
    processing().start()
    ui.dialog()