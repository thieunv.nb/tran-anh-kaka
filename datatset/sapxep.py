import cv2
import numpy as np 
import time
import glob
import joblib
from sklearn import svm
import os


size_default = (640, 480)
input_image_shape = (14,28,1)
model_svm = joblib.load("Model.joblib")


def get_features_vector(img):
    size= (14,28)
    img_resize = cv2.resize(img, size)
    row,col = img_resize.shape
    feature_vector = []
    for top_left_x in range(0,col):
        for top_left_y in range(0,row):
            current_val = int(img_resize[top_left_y, top_left_x])
            feature_vector.append(current_val)
    
    return feature_vector
def predict_svm(img):
    img_rz = cv2.resize(img, (14,28))
    _, img_threshold = cv2.threshold(img_rz, 0, 255, cv2.THRESH_OTSU)
    feature_vector = get_features_vector(img_threshold)
    result =  model_svm.predict([feature_vector])[0]
    return chr(result)


if __name__ == "__main__":
    files = glob.glob("cut\\*.jpg")
    for i in files:
        img =cv2.imread(i, 0)
        result = predict_svm(img)
        file_name = i.split('\\')[-1]
        folder = i.replace(file_name, "")

        new_path = "{}{}\\{}".format(folder, result,file_name )
        os.rename(i, new_path)