import pymongo as mongo
import time 
import datetime



class database():
    def __init__(self):
        self.conn = mongo.MongoClient("localhost", 27017)
        self.db = self.conn["license_plate"]
        self.db = self.db["data"]

    def check_id(self, car_id,license_plate, status = "Dang gui"):
        qer = {"car_id":car_id, "status" : "Dang gui"}
        docs = self.db.find(qer).limit(1)
        for doc in docs:
            if(doc["license_plate"]) == license_plate:
                return "PASS", "green"
            else:
                return "FAIL", "red"
        return "NOT FOUND", "orange"

    def add_new_car(self, car_id, license_plate, prices = 0, status = "Dang gui"):
        doc = {}
        time_in = datetime.datetime.now()
        doc["car_id"] = car_id
        doc["license_plate"] = license_plate
        doc["prices"] = prices
        doc["time_in"] = str(time_in)
        doc["time_out"] = "None"
        doc["status"] = status
        _count = self.db.find({"license_plate" :license_plate, "status":status }).count()
        __count = self.db.find({"car_id" :car_id, "status":status }).count()
        if(_count > 0):
            return -1
        else:
            if(__count > 0):
                return -2
            else:
                self.db.insert(doc)
                return 0

    def get_total_car(self):
        qer = {"status" : "Dang gui"}
        count = self.db.find(qer).count()
        return count
    def get_revenue(self):
        qer = {"status" : "Da lay xe"}
        docs = self.db.find(qer)
        revenue = 0
        for doc in docs:
            revenue += doc["prices"]
        return revenue

    def remove_car(self, car_id, license_plate, prices = 0, status = "Da lay xe"):
        h = 0
        m = 0
        s = 0
        scale = 0
        qer = {"car_id":car_id,  "license_plate" :license_plate, "status" : "Dang gui"}
        docs = self.db.find(qer).limit(1)
        for doc in docs:
            time_out = datetime.datetime.now()
            time_in = datetime.datetime.strptime(doc["time_in"], "%Y-%m-%d %H:%M:%S.%f")
            sub_time = time_out - time_in
            h = sub_time.seconds // 3600
            m = (sub_time.seconds//60)%60
            s = sub_time.seconds%60
            scale = h + m/60 + s / 3600
            prices = scale * prices
            prices = (prices // 1000)*1000 + 1000
            if(prices < 5000):
                prices = 5000

            doc["prices"] = prices
            doc["time_out"] = str(time_out)
            doc["status"] = status
            self.db.update(qer, doc)
            
            break
        return h,m,s,prices


if __name__ == "__main__":
    test = database()
    x = test.add_new_car("123456","35K123456")
    a, b = test.check_id("123456", "35K123456")
    print(a)
    if(a == "PASS"):
        test.remove_car("123456","35K123456")
    print(test.get_revenue())
    