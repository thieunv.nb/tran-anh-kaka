from sklearn import svm
import numpy as np
import joblib
from sklearn.model_selection import cross_val_score
from statistics import mean
import get_feature

def train():
    print('Reading data to training.........')
    # class2 = get_feature.get_features_2()
    print('Add label .........')
    x,y = get_feature.get_features_0()
    print('init SVM .........')
    clf = svm.SVC(kernel='linear', C = 10E8)
    print('Start train.........')
    score = cross_val_score(clf,x, y, cv = 5,scoring='accuracy')
    print("score:" + str( mean(score) * 100) + "%")
    
    clf.fit(x, y)
    joblib.dump(clf, 'Model.joblib')

if __name__ == '__main__':
    train()


