#include  <SPI.h>
#include <MFRC522.h>
int led = 13;
MFRC522 rfid(53,49);
MFRC522 rfid2(48,47);

void setup() {
  // put your setup code here, to run once:
  pinMode(led, OUTPUT);   
  Serial.begin(9600);
  SPI.begin();
  rfid.PCD_Init();
  rfid2.PCD_Init();
}

void loop() {
  rfid.PCD_Init();
  rfid2.PCD_Init();
    if(rfid.PICC_IsNewCardPresent())
    {
      if(rfid.PICC_ReadCardSerial())
      {
        Serial.print("i_");
        for( byte  i = 0; i < rfid.uid.size; i ++)
        {
          byte data = rfid.uid.uidByte[i];
          Serial.print(data , HEX);
        }
        Serial.println();
        rfid.PICC_HaltA();
        do
        {
          int inByte = Serial.read();
          if(inByte == 13)
          {
            break;
          }
        }
        while (!Serial.available());
        
        
      }
    }
    else if(rfid2.PICC_IsNewCardPresent())
    {
      if(rfid2.PICC_ReadCardSerial())
      {
        Serial.print("o_");
        for( byte  i = 0; i < rfid2.uid.size; i ++)
        {
          Serial.print(rfid2.uid.uidByte[i] , HEX);
        }
        Serial.println();
        rfid2.PICC_HaltA();
        do
        {
          int inByte = Serial.read();
          if(inByte == 13)
          {
            break;
          }
        }
        while (!Serial.available());
      }
    }
}
