import cv2
import numpy as np 
import time
import glob
import os


def distance(cnt):
    x,_,_,_ = cv2.boundingRect(cnt)
    return x

def segmenttation(img):
    img_char = []
    threshold_val = min(img.shape[0],img.shape[1] )
   
    if(threshold_val%2 == 0):
        threshold_val += 1
    img_mask = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,threshold_val,2)
    _, contours, _ = cv2.findContours(img_mask, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    mask = np.zeros((img.shape[0],img.shape[1]), np.uint8)
    mask = cv2.drawContours(mask, contours, 0, (255), -1)
    img_threshold = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,threshold_val,2)
    img_out = cv2.bitwise_and(mask, img_threshold)
    _, contours, _ = cv2.findContours(img_out, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=distance, reverse=False)
    for i in range(len(contours)):
        cnt = contours[i]
        x,y,w,h = cv2.boundingRect(cnt)
        if(h/w > 1.5 and cv2.contourArea(cnt) > 50):
            img_crop = img[y:y+h, x:x+w]
            img_char.append(img_crop)
    # cv2.imshow("threshold", img)
    # cv2.waitKey(0)
    return img_char
for item in fd:
    os.mkdir("cut\\" + str(item))
# if __name__ == "__main__":
#     files = glob.glob(r"D:\Outsources\tran-anh-kaka\datatset\licensedata\*.jpg")
#     for i in files:
#         img = cv2.imread(i, 0)
#         segmenttation(img)