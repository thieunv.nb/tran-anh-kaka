import numpy as np
import imutils
import cv2
import glob
# construct the argument parse and parse the arguments
def rotate(f):
    image = cv2.imread(f)
# loop over the rotation angles
    rotated = imutils.rotate_bound(image, -90)
    cv2.imwrite(f, rotated)


if __name__ == "__main__":
    files = glob.glob("licensedata\\*.jpg")
    for f in files:
        rotate(f)
        # break
