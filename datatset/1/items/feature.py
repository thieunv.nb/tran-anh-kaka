import cv2 as cv
import numpy as np 
import time
def get_features_vector(img):
    size= (14,28)
    img_resize = cv.resize(img, size)
    row,col = img_resize.shape
    feature_vector = []
    for top_left_x in range(0,col):
        for top_left_y in range(0,row):
            current_val = int(img_resize[top_left_y, top_left_x])
            feature_vector.append(current_val)
    
    return feature_vector

if __name__ == "__main__":
    features_matrix = []
    img = cv.imread("3.jpg", 1)
    feature_vector = get_features_vector(img)
    print (len(feature_vector))
    features_matrix.append(feature_vector)
    features_matrix = np.array(features_matrix, dtype = int)
    # np.savetxt('Positive_Matrix',features_matrix, fmt='%d')