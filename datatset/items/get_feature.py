import numpy as np 
import cv2 as cv
import glob
import feature

def get_features_0():
    img_path_list = glob.glob(r"dataset\*\*.*")
    features_matrix = []
    label = []
    for i, path in enumerate( img_path_list):
        print(str(i) +  " - reading "+ path)
        img = cv.imread(path, 0)
        _, img = cv.threshold(img, 0, 255, cv.THRESH_OTSU)
        feature_vector = feature.get_features_vector(img)
        features_matrix.append(feature_vector)
        lb = path.split('\\')[-2]
        label.append(ord(lb))
    features_matrix = np.array(features_matrix, dtype = int)
    label = np.asarray(label)
    return features_matrix, label
    
if __name__ == '__main__':
    pass
    