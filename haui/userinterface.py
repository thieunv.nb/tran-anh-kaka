import tkinter as tk
from tkinter import StringVar, filedialog, messagebox
import PIL
from PIL import Image, ImageTk
import cv2
import numpy as np
from haui import detector
import time

# size = (1400, 900)
size = (800,500)
w = size[0]
h = size[1]
input_image = None
output_image = None

input_license_image = None
output_license_image = None

class setup_ui(tk.Tk):
    def __init__(self, master,  maximum_car = 10):
        global size
        global w
        global h
        global input_image
        global output_image
        self.detector = detector.LicensePlateRecognition()
        self.maximum_car = maximum_car
        # khoi tao bien thay doi
        self.var_state_checking = StringVar()
        self.var_time = StringVar()
        self.var_price = StringVar()
        self.var_car_no = StringVar()
        self.var_revenue = StringVar()
        self.time_manual_input = time.time()
        self.time_manual_output = time.time()
        input_image = None
        output_image = None
        # khoi tao window dialog
        self.window = master
        self.window.title("Chương trình quản lý bãi đỗ xe")
        self.window.geometry(str(w) + 'x' + str(h))
        self.window.resizable(0,0)
        # khoi tao groupbox ngo vao
        self.g_image1 = tk.LabelFrame(self.window, text="Camera ô tô vào")
        self.imagebox_input = tk.Label(self.g_image1)
        self.button_browser_input = tk.Button(master=self.g_image1, text ="...", command = self.browser_event_input)
        # khoi tao groupbox ngo ra
        self.g_image2 = tk.LabelFrame(self.window, text="Camera ô tô ra")
        self.imagebox_output = tk.Label(self.g_image2)
        self.button_browser_output = tk.Button(master=self.g_image2, text ="...", command = self.browser_event_output)
        # khoi tao groupbox xu ly
        self.g_handled = tk.LabelFrame(self.window, text="Thông tin và xử lý")
        self.imagebox_place_input = tk.Label(self.g_handled)
        self.imagebox_place_output = tk.Label(self.g_handled)
        self.textbox_place_input = tk.Text(self.g_handled)
        self.textbox_place_output = tk.Text(self.g_handled)
        # label show status and computed
        self.label_state = tk.Label(self.g_handled)
        self.label_time = tk.Label(self.g_handled)
        self.label_price = tk.Label(self.g_handled)
        self.label_time_val = tk.Label(self.g_handled)
        self.label_price_val = tk.Label(self.g_handled)
        self.label_car_no = tk.Label(self.g_handled)
        self.label_revenue = tk.Label(self.g_handled)
        self.label_car_no_val = tk.Label(self.g_handled)
        self.label_revenue_val = tk.Label(self.g_handled)
        # khoi tao ui
        self.init_ui()
        self.set_state("Ready", "deep sky blue")
        self.set_time_label(hours = 0, minutes=0, seconds=0)
        self.set_prices_label(prices=150000)

    def init_ui(self):
        global size
        global w
        global h
        global input_image
        global output_image
        global input_license_image
        global output_license_image
        # tạo group box input camera
        self.g_image1.place(x=0, y=0, width=int(w/2),height=int(h/2))
        self.imagebox_input.place(x=0, y=0, width=int(w/2 - 5),height=int(h/2 -20))
        self.imagebox_output.config(image = input_image)
        self.button_browser_input.place(x=int(w/2) - int(w/15) , y=0, width=int(w/20),  height=int(h/20))
        self.button_browser_input.configfont=("Arial", int(min(h, w)/35))
        # self.imagebox_output.pack()
        # tao groub box output camera
        self.g_image2.place(x=0, y = int(h/2), width=int(w/2), height=int(h/2))
        self.imagebox_output.place(x=0, y=0, width=int(w/2 - 5),height=int(h/2 -20))
        self.imagebox_output.config( image = output_image)
        self.button_browser_output.place(x=int(w/2) - int(w/15) , y=0, width=int(w/20),  height=int(h/20))
        self.button_browser_output.configfont=("Arial", int(min(h, w)/35))
        # self.imagebox_output.pack()

        # group handle
        # # imagebox
        self.g_handled.place(x= int(w/2), y = 0, width=int(w/2), height=int(h))
        self.imagebox_place_input.place(x=0, y=0, width=int(w/4 - 5),height=int(h/4))
        self.imagebox_place_input.config( background="powder blue")
        # self.imagebox_place_input.pack()
        self.imagebox_place_output.place(x=int( w/4), y=0, width=int( w/4 - 5),height=int(h/4))
        self.imagebox_place_output.config( background="powder blue")
        # self.imagebox_place_output.pack()
        ## textbox
        self.textbox_place_input.place(x = 0, y=int(h/4) + 5, width=int(w/4 - 5), height = int(min(h, w)/25))
        self.textbox_place_input.config(font=("Arial", int(min(h, w)/50)))
        # self.textbox_place_input.config(background="blue")
        self.textbox_place_output.place(x = int(w/4), y=int(h/4) + 5, width=int(w/4 - 5), height = int(min(h, w)/25))
        self.textbox_place_output.config(font=("Arial", int(min(h, w)/50)))
        # self.textbox_place_input.config(background="blue"))


        self.label_state.place(x = int(w/8), y = int(h/3) , width = int(w/4), height = int(h/8))
        self.label_state.config(background="green", textvariable = self.var_state_checking,  font=("Arial", int(min(h, w)/25)))
        self.label_time.place(x = int(w/30), y = int(3*h/5 - h/10) , width = int(w/11), height = int(h/25))
        self.label_time.config( text = "Thời gian:",  font=("Arial", int(min(h, w)/50)))
        self.label_time_val.place(x = int(w/8), y = int(3*h/5 - h/10) , width = int(w/4),   height = int(h/25))
        self.label_time_val.config(textvariable = self.var_time,  font=("Arial", int(min(h, w)/50)), foreground="red4")

        self.label_price.place(x = int(w/30), y = int(3.1*h/5) , width = int(w/12), height = int(h/25))
        self.label_price.config( text = "Giá:",  font=("Arial", int(min(h, w)/35)))
        
        self.label_price_val.place(x = int(w/10), y = int(3*h/5) , width = int(1.2 *w/4),   height = int(h/15))
        self.label_price_val.config( textvariable = self.var_price, font=("Arial", int(min(h, w)/25)), foreground="red4")

        self.label_car_no.place(x = int(w/30), y = int(3.8*h/5 ) , width = int(w/10), height = int(h/25))
        self.label_car_no.config( text = "Tổng số xe:",  font=("Arial", int(min(h, w)/50)), foreground="royal blue")
        self.label_car_no_val.place(x = int(w/6), y = int(3.8*h/5 ) , width = int(w/4),   height = int(h/25))
        self.label_car_no_val.config( textvariable = self.var_car_no, font=("Arial", int(min(h, w)/50)), foreground="royal blue")
        self.label_revenue.place(x = int(w/30), y = int(4.2*h/5 ) , width = int(w/10), height = int(h/25))
        self.label_revenue.config( text = "Doanh thu:",  font=("Arial", int(min(h, w)/50)), foreground="blue")
        self.label_revenue_val.place(x = int(w/6),  y = int(4.2*h/5 ) , width = int(w/4),   height = int(h/25))
        self.label_revenue_val.config( textvariable = self.var_revenue, font=("Arial", int(min(h, w)/40)), foreground="blue")

    def browser_event_input(self):
        file_name = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
        try:
            img = cv2.imread(file_name, 0)
            if(img is not None):
                self.time_manual_input = time.time()
                img_plate, result =  self.detector.license_plate_recognition(img)
                self.set_image_input(img, manual=True)
                
                if(img_plate is not None):
                    self.set_image_license_input(img_plate)
                    self.set_plate_car_input(result)
                else:
                    self.set_image_license_input(None)
                    self.set_plate_car_input("")
                    messagebox.showerror( "Lỗi", "Không tìm thấy biển số")
        except:
            pass
    def browser_event_output(self):
        file_name = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
        try:
            img = cv2.imread(file_name, 0)
            if(img is not None):
                self.time_manual_output = time.time()
                img_plate, result =  self.detector.license_plate_recognition(img)
                self.set_image_output(img, manual=True)
                if(img_plate is not None):
                    self.set_image_license_output(img_plate)
                    self.set_plate_car_output(result)
                else:
                    self.set_image_license_output(None)
                    self.set_plate_car_output("")
                    messagebox.showerror( "Lỗi", "Không tìm thấy biển số")
        except:
            pass

    def set_plate_car_input(self, plate_car):
        print("[INFO] Changed state license plate input to : " + plate_car)
        self.textbox_place_input.delete('1.0', tk.END)
        self.textbox_place_input.insert(tk.END , plate_car)
    def set_plate_car_output(self, plate_car):
        print("[INFO] Changed state license plate output to : " + plate_car)
        self.textbox_place_output.delete('1.0', tk.END)
        self.textbox_place_output.insert(tk.END , plate_car)
    def set_state(self, status, color = "green"):
        print("[INFO] Changed state to : " + status)
        self.var_state_checking.set(status)
        self.label_state.config(background=color)

    def set_image_output(self, img, manual = False):
        global w
        global h
        global output_image
        if(time.time() - self.time_manual_output > 10 or manual == True):
            img_rz = cv2.resize(img, (int(w/2 - 5),int(h/2 -20)))
            img_arr = Image.fromarray(img_rz)
            photo = ImageTk.PhotoImage(img_arr)
            output_image = photo
            self.imagebox_output.config(image = output_image)
        # self.imagebox_output.pack()
    def set_image_input(self, img, manual = False):
        global w
        global h
        global input_image
        if(time.time() - self.time_manual_input > 10 or manual == True):
            img_rz = cv2.resize(img, (int(w/2 - 5),int(h/2 -20)))
            img_arr = Image.fromarray(img_rz)
            photo = ImageTk.PhotoImage(img_arr)
            input_image = photo
            self.imagebox_input.config(image = input_image)
        # self.imagebox_output.pack()

    def set_image_license_input(self, img):
        global w
        global h
        global input_license_image
        if(img is not None):
            scale = int(w/4 - 5) / img.shape[1]
            height_img = int(scale * img.shape[0])
            img_rz = cv2.resize(img, (int(w/4 - 5),height_img))
            img_arr = Image.fromarray(img_rz)
            photo = ImageTk.PhotoImage(img_arr)
            input_license_image = photo
            self.imagebox_place_input.config(image = input_license_image)
        else:
            input_license_image = None
            self.imagebox_place_input.config(image = input_license_image)
    
    def set_image_license_output(self, img):
        global w
        global h
        global output_license_image
        if(img is not None):
            scale = int(w/4 - 5) / img.shape[1]
            height_img = int(scale * img.shape[0])
            img_rz = cv2.resize(img, (int(w/4 - 5),height_img))
            img_arr = Image.fromarray(img_rz)
            photo = ImageTk.PhotoImage(img_arr)
            output_license_image = photo
            self.imagebox_place_output.config(image = output_license_image)
        else:
            output_license_image = None
            self.imagebox_place_output.config(image = output_license_image)
    def full_slot(self):
        messagebox.showerror( "Lỗi", "Bãi đỗ đã hết chỗ trống!")
        time.sleep(1)
    def not_found(self):
        messagebox.showerror( "Lỗi", "Không tìm thấy biển số, thử lại!")
        time.sleep(1)

    def card_id_exists(self):
        pass
        messagebox.showerror( "Lỗi", "Thẻ này đã được sử dụng!")
        time.sleep(1)
    
    def car_exists(self):
        pass
        messagebox.showerror( "Lỗi", "Xe này đang trong bãi!")
        time.sleep(1)

    def set_time_label(self, hours = 0, minutes = 0, seconds = 0):
        str_time = "{} giờ {} phút {} giây".format(hours, minutes, seconds)
        self.var_time.set(str_time)
    def set_prices_label(self, prices = 0):
        str_prices = "{:,.0f} đ".format(prices)
        self.var_price.set(str_prices)

    def set_car_no_label(self, no_car = 0):
        str_car_no = "Có {} / {} xe trong bãi!".format(no_car, self.maximum_car)
        self.var_car_no.set(str_car_no)

    def set_revenue(self, revenue = 0):
        str_var_revenue = "{:,.0f} đ".format(revenue)
        self.var_revenue.set(str_var_revenue)
    
    def dialog(self):
        self.window.mainloop() 


if __name__ == "__main__":
    frame = tk.Tk()
    ui = setup_ui(frame, maximum_car = 50)
    # img = cv2.imread("1.jpg", 0)
    # ui.set_image_license_input(img)
    # ui.set_image_license_output(img)
    ui.set_state("Ready", "deep sky blue")
    ui.set_time_label(hours = 0, minutes=0, seconds=0)
    ui.set_prices_label(prices=5000)
    ui.set_car_no_label(0)
    ui.set_revenue(6546516354)
    ui.dialog()
    
    

