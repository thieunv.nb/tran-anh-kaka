from serial import Serial
import time

class arduino():
    def __init__(self):
        self.s = Serial("/dev/ttyUSB0", 9600)
    def get_data_from_arduino(self):
        data_str = ""
        while True:
            data_byte = self.s.read(1)
            if(data_byte == b'\n'):
                break
            else:
                data_str += data_byte.decode()
        direction, id_card  = data_str.split('_')[0], data_str.split('_')[1]
        return direction,id_card
    def done(self):
        self.s.write(b'\r')
    def release(self):
        self.s.read_all()
        self.s.write(b'\r')
if __name__ == "__main__":
    while 1:
        ar = arduino()
        a, b = ar.get_data_from_arduino()
        print(a,b)