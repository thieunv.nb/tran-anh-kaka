import cv2
import numpy as np 
import time
import glob
import joblib
from sklearn import svm
size_default = (640, 480)
input_image_shape = (14,28,1)
model_svm = joblib.load("Model.joblib")

def get_features_vector(img):
    size= (14,28)
    img_resize = cv2.resize(img, size)
    row,col = img_resize.shape
    feature_vector = []
    for top_left_x in range(0,col):
        for top_left_y in range(0,row):
            current_val = int(img_resize[top_left_y, top_left_x])
            feature_vector.append(current_val)
    
    return feature_vector
def predict_svm(img):
    img_rz = cv2.resize(img, (14,28))
    _, img_threshold = cv2.threshold(img_rz, 0, 255, cv2.THRESH_OTSU)
    feature_vector = get_features_vector(img_threshold)
    result =  model_svm.predict([feature_vector])[0]
    return chr(result)

def ocr_license(listimg):
    license_plate = ""
    for img in listimg:
        
        license_plate += predict_svm(img)

    return license_plate

def distance(cnt):
    x,_,_,_ = cv2.boundingRect(cnt)
    return x


def segmenttation(img):
    img_char = []
    threshold_val = min(img.shape[0],img.shape[1] )
   
    if(threshold_val%2 == 0):
        threshold_val += 1
    img_mask = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,threshold_val,2)
    _, contours, _ = cv2.findContours(img_mask, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    mask = np.zeros((img.shape[0],img.shape[1]), np.uint8)
    mask = cv2.drawContours(mask, contours, 0, (255), -1)
    img_threshold = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,threshold_val,2)
    img_out = cv2.bitwise_and(mask, img_threshold)
    _, contours, _ = cv2.findContours(img_out, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=distance, reverse=False)
    for i in range(len(contours)):
        cnt = contours[i]
        x,y,w,h = cv2.boundingRect(cnt)
        if(h/w > 1.5 and cv2.contourArea(cnt) > 50):
            img_crop = img[y:y+h, x:x+w]
            img_char.append(img_crop)
    return img_char




def license_plate_recognition(img, min_size = 8000, max_size = 120000):
    img_roi = None
    result = ""
    img_rz = cv2.resize(img ,size_default)
    img_blur = cv2.bilateralFilter(img_rz, 9, 17, 17)
    img_canny = cv2.Canny(img_blur, 100,200) #cv2.Laplacian(img_blur, cv2.CV_8U, ksize=5)
    _, img_threshold = cv2.threshold(img_canny, 0,255, cv2.THRESH_OTSU)
    _, contours, _ = cv2.findContours(img_threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse = True)
    
    # license_cnt = None
    for cnt in contours:
        peri = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.05 * peri, True)
        s_cnt =  cv2.contourArea(cnt)
        if(len(approx)== 4 and max_size > s_cnt > min_size):
            x,y,w,h = cv2.boundingRect(cnt)
            img_temp = img_threshold[y:y+h, x:x+w]
            count_nzero = cv2.countNonZero(img_temp)
            if(count_nzero/s_cnt > 0.1):
                img_temp = img_rz[y:y+h, x:x+w]
                img_chr = segmenttation(img_temp)
                if(img_chr != []):
                    result = ocr_license(img_chr)
                    if(result != ""):
                        img_roi = img_temp
                        break
    return img_roi, result

if __name__ == "__main__":
    # img = cv2.imread("45.jpg", 0)
    # x = predict_svm(img)
    # print(x)
    cap = cv2.VideoCapture(0)
    while 1:
        _, frame = cap.read()
        img_gray  = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        img_roi , result = license_plate_recognition(img_gray)
        if(img_roi is not None):
            cv2.destroyAllWindows()
            cv2.imshow(result, img_roi)
        
        cv2.waitKey(1)
        # break
    



























# def load_data():
#     files = glob.glob("items\\dataset\\*\\*.jpg")
#     x = []
#     y = []
#     lb = ""
#     for i, f in enumerate(files):
#         char = f.split('\\')[-2]
#         label = ord(char)
#         if(label not in lb):
#             lb += label
#         img = cv2.imread(f, 0)
#         img_rz = cv2.resize(img, (input_image_shape[0], input_image_shape[1]))
#         x.append(img_rz)
#         y.append(lb.index(label))
#     x = np.asarray(x)
#     x = x.reshape(x.shape[0],input_image_shape[0], input_image_shape[1], input_image_shape[2] )
#     x = x.astype('float32')/ 255.0
#     y = np.asarray(y)
#     return x, y

# def model_dl():
#     import keras
#     from keras import layers
#     model = keras.Sequential()
#     model.add(layers.Conv2D(filters=6, kernel_size=(3, 3), activation='relu', input_shape=input_image_shape))
#     model.add(layers.AveragePooling2D())
#     model.add(layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu'))
#     model.add(layers.AveragePooling2D())
#     model.add(layers.Flatten())

#     model.add(layers.Dense(units=120, activation='relu'))
#     model.add(layers.Dense(units=84, activation='relu'))
#     model.add(layers.Dense(units=32, activation = 'softmax'))
#     opt = keras.optimizers.SGD(lr = 1e-4, decay = 1e-4,momentum= 0.9, nesterov= True)
#     model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer=opt)
#     return model

# def train_model():
#     from matplotlib import pyplot as plt
#     from sklearn.model_selection import train_test_split
#     from keras.utils import to_categorical
#     import keras
#     from keras import layers
#     x, y = load_data()
#     y = to_categorical(y)
#     print(y)
#     model = model_dl()
#     x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.1)
#     H = model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs= 256, batch_size = 32)
#     del x
#     del y
#     del x_train
#     del y_train
#     del x_test
#     del y_test
#     model.save_weights("char.h5")
#     print(H.history.keys())
#     # evaluate the network
#     print("[INFO] analsys network...")
#     # plot the training loss and accuracy
#     # 10 inch x 6 inch
#     plt.figure(figsize=(12, 6))
#     plt.subplot(121)
#     plt.plot(H.history['accuracy'])
#     plt.plot(H.history['val_accuracy'])
#     plt.title('[INFO] model accuracy')
#     plt.ylabel('accuracy')
#     plt.xlabel('epoch')
#     plt.legend(['train', 'test'], loc='upper left')
#     plt.subplot(122)
#     plt.plot(H.history['loss'])
#     plt.plot(H.history['val_loss'])
#     plt.title('[INFO] model loss')
#     plt.legend(['train_loss', 'test_loss'], loc='upper left')
#     plt.show()