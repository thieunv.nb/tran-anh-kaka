import os
import glob
import cv2

files = glob.glob("data\\*.jpg")
for f in files:
    img = cv2.imread(f, 0)
    char = f.split('\\')[-1].split('.')[0]
    os.mkdir("dataset\\" + char)
    _, threshold = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)
    _, contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for i in range(0, len(contours)):
        if(1500 > cv2.contourArea(contours[i], False) > 100):
            x,y,w,h = cv2.boundingRect(contours[i])
            item = img[y:y+h, x:x+w]
            cv2.imwrite("dataset\\"+char+ "\\" + str(i) + ".jpg", item)